import { BasicTodoCliPage } from './app.po';

describe('basic-todo-cli App', () => {
  let page: BasicTodoCliPage;

  beforeEach(() => {
    page = new BasicTodoCliPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
