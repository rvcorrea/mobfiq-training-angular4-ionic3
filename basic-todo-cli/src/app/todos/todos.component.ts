import { Component, OnInit } from '@angular/core';
import { Todo } from "app/todos/todo.model";

@Component({
  selector: 'todos-list',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
    todoList:Array<Todo> = new Array<Todo>();

    constructor() { }

    ngOnInit() { 
        let savedTodos = localStorage.getItem("todos-list");
        if(savedTodos) {
            this.todoList = JSON.parse(savedTodos);
        }
    }

    addNewTodo()
    {
        let title = prompt("Criar novo lembrete");
        if(title)
        {
            this.todoList.push(new Todo(title));            
            this.saveToStorage();
        }
    }

    editTodo(event:any, todoToEdit:Todo)
    {
        event.stopPropagation();
        let title = prompt("Editar lembrete", todoToEdit.title);
        if(title && title != todoToEdit.title)
        {
            todoToEdit.title = title;            
            this.saveToStorage();
        }
    }

    removeTodo(index: number)
    {
        this.todoList.splice(index, 1);
        this.saveToStorage();
    }

    toggleStatus(todo:Todo)
    {
        todo.completed = !todo.completed;
        this.saveToStorage();
    }

    saveToStorage()
    {
        localStorage.setItem("todos-list", JSON.stringify(this.todoList));
    }

}