import { NgModule } from '@angular/core';

import { TodosComponent } from './todos.component';
import { CommonModule } from "@angular/common";

@NgModule({
    imports: [ CommonModule ],
    exports: [ TodosComponent ],
    declarations: [ TodosComponent ],
    providers: [],
})
export class TodosModule { }
