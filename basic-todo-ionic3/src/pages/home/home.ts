import { Component } from '@angular/core';
import { NavController, ActionSheetController, AlertController } from 'ionic-angular';
import { Todo } from "../../models/todo.model";

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  todosList: Array<Todo> = new Array<Todo>();

  constructor(public navCtrl: NavController, private storage: Storage, public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController) {}
  
  ionViewDidLoad()
  {
    this.storage.get("basic-todo-items").then((todoList)=>{
      if(todoList) {
        this.todosList = JSON.parse(todoList);
      }
    });
  }

  presentMenu(todo:Todo) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opções',
      buttons: [
      {
          text: todo.completed ? "Desfazer":"Fazer",
          handler: () => {
            todo.completed = !todo.completed;
            this.saveToStorage();
          }
        },
        {
          text: 'Editar',
          handler: () => {
            this.showPrompt(todo);
          }
        },
        {
          text: 'Apagar',
          role: 'destructive',
          handler: () => {
            this.todosList.splice(this.todosList.indexOf(todo), 1);
            this.saveToStorage();
          }
        }
      ]
    });
    actionSheet.present();
  }

  showPrompt(todo?:Todo) {
    let prompt = this.alertCtrl.create({
      title: todo ? 'Editar lembrete' : 'Novo lembrete',
      message: "Entre com o que voce deseja lembrar",
      inputs: [
        {
          name: 'title',
          placeholder: 'Digite aqui',
          value: todo ? todo.title : '',
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Salvar',
          handler: data => {
            if(todo)
              todo.title = data.title;
            else
              this.createTodo(data.title);
            
            this.saveToStorage();
          }
        }
      ]
    });
    prompt.present();
  }

  createTodo(text?:string)
  {
    if(!text)
      return;

    this.todosList.push(new Todo(text));
  }

  saveToStorage()
  {
    this.storage.set("basic-todo-items", JSON.stringify(this.todosList));
  }
}
