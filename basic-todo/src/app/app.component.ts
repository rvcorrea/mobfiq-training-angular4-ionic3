import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <h1>Lista de lembretes</h1>
    <hr>
    <todos-list></todos-list>
  `
})
export class AppComponent  { name = 'Angular'; }
