import { CreateUserPage } from './../pages/create-user/create-user';
import { LoginPage } from './../pages/login/login';
import { MorePage } from './../pages/more/more';
import { CartPage } from './../pages/cart/cart';
import { ProfilePage } from './../pages/profile/profile';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { IonicStorageModule } from '@ionic/storage';
import { CategoriesProvider } from '../providers/categories/categories';
import { ProfileProvider } from '../providers/profile/profile';
import { CartProvider } from '../providers/cart/cart';
import { CategoriesPage } from "../pages/categories/categories";
import { CommonModule } from "@angular/common";
import { HttpModule } from "@angular/http";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    CategoriesPage,
    CartPage,
    MorePage  ],
  imports: [
    BrowserModule,
    CommonModule,
    IonicModule.forRoot(MyApp,{
      mode: "ios",
      pageTransition: 'ios-transition' 
    }),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    CategoriesPage,
    CartPage,
    MorePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CategoriesProvider,
    ProfileProvider,
    CartProvider
  ]
})
export class AppModule {}
