export class Login {
    Email:string;
    Password:string;
    Pin:string;
    Token:string;

    constructor(Token?:string)
    {   
        if(Token)
            this.Token = Token;
    }
}