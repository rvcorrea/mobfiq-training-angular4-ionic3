import { ProfileProvider } from './../../providers/profile/profile';
import { Client } from './../../models/client.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the CreateUserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-create-user',
  templateUrl: 'create-user.html',
})
export class CreateUserPage {
  private newClient: Client = new Client();

  constructor(public navCtrl: NavController, public navParams: NavParams, private profileProvider: ProfileProvider, public toastCtrl: ToastController) {
  
  }

  async createNewClient()
  {
    try{
      let newClient = await this.profileProvider.createClient(this.newClient);
      if(newClient) {

      }
    } catch(error) {
      let errorBody = JSON.parse(error._body);
      this.showToast(errorBody.Message);
    }
    
  }

  showToast(text:string)
  {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position:"top"
      });
      toast.present();
  }

}
