import { ProfilePage } from './../profile/profile';
import { ProfileProvider } from './../../providers/profile/profile';
import { Login } from './../../models/login.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private Email:string;
  private Password:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private profileProvider: ProfileProvider, public toastCtrl: ToastController, private storage: Storage){
  }

  async login()
  {
    try{
      let loggedClient = await this.profileProvider.login(this.Email, this.Password);
      if(loggedClient) {
        this.storage.set("LoggedClient", JSON.stringify(loggedClient));
        this.showToast("Login efetuado com succeso!");
        this.navCtrl.setRoot("ProfilePage");
      }
    } catch(error) {
      let errorBody = JSON.parse(error._body);
      this.showToast(errorBody.Message);
    }
    
  }

  newClient()
  {
    this.navCtrl.push("CreateUserPage");
  }

  showToast(text:string)
  {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position:"top"
      });
      toast.present();
  }
}
