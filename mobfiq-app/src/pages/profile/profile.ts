import { Login } from './../../models/login.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ionViewDidEnter() {
    this.storage.get("LoggedClient").then((response) => {
      if(!response) {
        this.navCtrl.setRoot("LoginPage");
      }
    });
  }

  logout() {
    this.storage.set("LoggedClient", null);
    this.navCtrl.setRoot("LoginPage");
  }
}
