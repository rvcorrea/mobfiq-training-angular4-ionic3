import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { CategoriesPage } from "../categories/categories";
import { ProfilePage } from "../profile/profile";
import { CartPage } from './../cart/cart';
import { MorePage } from './../more/more';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = CategoriesPage;
  tab3Root = "ProfilePage";
  tab4Root = CartPage;
  tab5Root = MorePage;

  constructor() {

  }
}
