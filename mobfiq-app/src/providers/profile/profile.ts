import { Login } from './../../models/login.model';
import { Client } from './../../models/client.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProfileProvider {
  private ProfileEndpoints = {
    "base":"http://api-homolog-polishopv2.mobfiq.com.br/",
    "login":"pwa/login",
    "info":"pwa/client/get",
    "orders":"pwa/client/orders",
    "new":"pwa/client/new",
    "validate":"pwa/client/validate"
  };

  constructor(public http: Http) {  
  }

  async login(Email:string, Password:string) : Promise<Client>
  {
    let credentials = new Login();
    credentials.Email = Email;
    credentials.Password = Password;

    return this.http.post(this.ProfileEndpoints.base + this.ProfileEndpoints.login, credentials).map(response => response.json()).toPromise();
  }

  async getClientInfo(ClientToken:string) : Promise<Client>
  {
    let credentials = new Login();
    credentials.Token = ClientToken;

    return this.http.post(this.ProfileEndpoints.base + this.ProfileEndpoints.info, credentials).map(response => response.json()).toPromise();
  }

  async createClient(Client:Client) : Promise<Client>
  {
    return this.http.post(this.ProfileEndpoints.base + this.ProfileEndpoints.new, Client).map(response => response.json()).toPromise();
  }

  async validateClient(Email:string, Pin:string, Password:string) : Promise<Client>
  {
    let credentials = new Login();
    credentials.Email = Email;
    credentials.Pin = Pin;
    credentials.Password = Password;

    return this.http.post(this.ProfileEndpoints.base + this.ProfileEndpoints.validate, credentials).map(response => response.json()).toPromise();
  }



}
